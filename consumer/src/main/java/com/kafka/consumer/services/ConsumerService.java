package com.kafka.consumer.services;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class ConsumerService {


    @KafkaListener(topics = "java_test", groupId = "group_id")
    public void consume(String message){
        System.out.println("New event emitted");
        System.out.println(message);
    }
}
