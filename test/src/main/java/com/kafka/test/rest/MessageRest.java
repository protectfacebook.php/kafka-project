package com.kafka.test.rest;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.kafka.test.model.Message;
import com.kafka.test.service.IMessageService;
import com.kafka.test.service.common.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class MessageRest {

    private final IMessageService messageService;

    @Autowired
    public MessageRest(IMessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping("/message")
    public ResponseEntity<CommonResponse<String>> message(@RequestBody Message message) throws JsonProcessingException {
        try{
            messageService.message(message);
            CommonResponse<String> response = CommonResponse.<String>builder()
                    .status(200)
                    .success(true)
                    .response("Message sent")
                    .build();
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            CommonResponse<String> response = CommonResponse.<String>builder()
                    .status(200)
                    .success(true)
                    .response(e.getMessage())
                    .build();
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
    }
}
