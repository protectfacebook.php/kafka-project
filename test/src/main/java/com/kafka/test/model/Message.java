package com.kafka.test.model;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;

@Builder
@Value
public class Message implements Serializable {
    private String id;
    private String name;
    private String message;
}
