package com.kafka.test.service.common;

import lombok.Builder;

import java.io.Serializable;

@Builder
public class CommonResponse<T> implements Serializable {
    private Integer status;
    private Boolean success;
    private T response;
}
