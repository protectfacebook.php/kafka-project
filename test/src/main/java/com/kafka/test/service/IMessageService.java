package com.kafka.test.service;

import com.kafka.test.model.Message;

public interface IMessageService extends CommonDAO<Message, Message> {
}
