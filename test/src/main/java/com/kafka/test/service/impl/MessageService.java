package com.kafka.test.service.impl;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kafka.test.config.Constants;
import com.kafka.test.model.Message;
import com.kafka.test.service.IMessageService;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class MessageService  implements IMessageService {

    private final KafkaTemplate<String, String> template;
    private final ObjectMapper objectMapper;

    public MessageService(KafkaTemplate<String, String> template, ObjectMapper objectMapper) {
        this.template = template;
        this.objectMapper = objectMapper;
    }


    @Override
    public Message message(Message message)  {
        send(Constants.Topic.TEST_TOPIC, message);
        return message;
    }

    private void send(String topic, Message message) {
        try {
            ProducerRecord<String, String> record =
                    new ProducerRecord<>(topic, message.getId(),  objectMapper.writeValueAsString(message));
            template.send(record).addCallback(
                    succes -> {
                        System.out.println("Mensaje enviado");
                    },
                    err -> {
                        System.out.println("Error en el envio");
                    }
            );
        }catch (Exception e){
            System.out.println("Error enviando: ");
            System.out.println(e);
        }
    }
}
