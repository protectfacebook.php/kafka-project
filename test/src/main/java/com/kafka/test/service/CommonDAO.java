package com.kafka.test.service;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface CommonDAO<T,D> {
    T message (D d) throws JsonProcessingException;
}
